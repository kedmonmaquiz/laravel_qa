<?php

namespace App;

use App\Question;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{

    protected $fillable = ['body','user_id'];

    public function question(){
        return $this->belongsTo(Question::class);
    }

     public function user(){
        return $this->belongsTo(User::class);
    }

    public function getBodyHtmlAttribute(){
    	return \Parsedown::instance()->text($this->body);
    }

    public static function boot(){
    	parent::boot();

    	static::created(function($answer){
            $answer->question->increment('answers_count');
    	});

        static::deleted(function($answer){
            $answer->question->decrement('answers_count');
        });
    }

    public function getCreatedDateAttribute(){
    	return $this->created_at->diffForHumans();
    }

    public function getStatusAttribute(){
        if($this->id===$this->question->best_answer_id){
            return 'vote-accepted';
        }
        return '';
    }

    public function getIsBestAttribute(){
        if($this->id===$this->question->best_answer_id){
            return true;
        }else{
            return false;
        }
    }
}
