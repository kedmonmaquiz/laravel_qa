<?php

namespace App;

use App\Answer;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['title','body'];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function setTitleAttribute($value){
    	$this->attributes['title'] = $value;
    	$this->attributes['slug'] = str_slug($value);
    }

    public function getStatusAttribute(){
    	if($this->answers_count>0){
    		if($this->best_answer_id){
    			return 'answer-accepted';
    		}
    		return 'answered';
    	}

    	return 'unanswered';
    }

    public function getBodyHtmlAttribute(){
        return \Parsedown::instance()->text($this->body);
    }

    public function answers(){
        return $this->hasMany(Answer::class);
    }

    public function getCreatedDateAttribute(){
        return $this->created_at->diffForHumans();
    }

    public function acceptBestAnswer($answer){
            $this->best_answer_id = $answer->id;
            $this->save();       
    }

    public function favourites(){
        return $this->belongsToMany(User::class,'favourites');
    }

    public function getFavouritesCountAttribute(){
        return $this->favourites->count();
    }

    public function getIsFavouritedAttribute(){
        if(\Auth::guest()){
            return 'off';
        }else{
            $users_favoured = $this->favourites->pluck('id');
            // $sum = 0;
            // for($i=0; $i < count($users_favoured); $i++){
            //  if($users_favoured[$i] === \Auth::id()){
            //     $sum +=1;
            //  }
            // }

            // if($sum > 0 ){
            //     return 'favourited';
            // }

            //This is the altenative of the commented statements above
            if($users_favoured->contains(\Auth::id())){
                return 'favourited';
            }
        }
    }

}
