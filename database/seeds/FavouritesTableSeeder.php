<?php

use App\Question;
use App\User;
use Illuminate\Database\Seeder;

class FavouritesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('favourites')->delete();
        $users = User::pluck('id')->all();
        $numberOfUsers = count($users);
        $questions =Question::all();

        foreach($questions as $question){
        	for ($i=0; $i < rand(0,$numberOfUsers); $i++) { 
        		$user=$users[$i];
        		$question->favourites()->attach($user); 
        	}
        }
    }
}
