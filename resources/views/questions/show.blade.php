@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="card-title">
                    <div class="d-flex">
                       <h1 class="mr-2">{{$question->title}}</h1>
                       <div class="ml-auto">
                          <a href="{{route('questions.index')}}" class="btn btn-outline-primary">Back</a>   
                       </div>    
                    </div>    
                </div>
                <div class="media">
                  <div class="d-flex flex-column vote-controls">
                    <a title="This question is useful" class="vote-up">
                     <i class="fas fa-caret-up fa-3x"></i>
                    </a>
                    <span class="votes-count">1230</span>
                    <a title="This question is not useful" class="vote-down off">
                      <i class="fas fa-caret-down fa-3x"></i>
                    </a>
                    <a title="Click to mark as favourite question(Click again to undo)" class="favourite mt-2 {{$question->is_favourited}}" onclick="
                        event.preventDefault();
                        document.getElementById('form-favourites-{{$question->id}}').submit();">
                      <i class="fas fa-star fa-2x"></i>
                      <span class="favourite-count">{{$question->favourites_count}}</span>
                    </a>
                    <form action="{{route('questions.favourites',$question->id)}}" method="post" id="form-favourites-{{$question->id}}" style="display: none">
                      @csrf
                      @if ($question->is_favourited)
                        @method('DELETE')
                      @endif
                    </form>

                  </div>
                  <div class="media-body">
                    {!! $question->body_html !!}
                      <div class="float-right mr-2">
                        <span class="text-muted"> created {{$question->created_date}}</span>
                        <div class="media mt-2">
                          <a href="#" class="pr-2"><img src="{{asset('images/user.png')}}" class="img-rounded" alt="" width= "30px" height="30px"></a>
                          <div class="media-body mt-1">
                            <a href="#">{{$question->user->name}}</a>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>

   @include('answers._index',[
      'answers'=>$question->answers,
      'answersCount'=>$question->answers_count
    ])

    @include('answers._create')
</div>
@endsection